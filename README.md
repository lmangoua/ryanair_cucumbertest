# Ryanair Assessment: Automation Cucumber framework#

## TOOLS USED ##

Mac OS, IntelliJ IDE(version 11.0.11), Java(version 11.0.9), Maven(version 3.6.3), TestNG.

---

## FRAMEWORK STRUCTURE ##

### pom.xml file ###
The pom.xml file found on the **Automation framework root folder** contains all the common dependencies that are used in the project.

### engine ###
Contains functionality that form the backbone of the framework.

**Hook.java**
- contains configurations to setup browser's driver, logger and teardown.

### implementation ###
Contains classes with implemented methods for every actions done per pages.

**Basket.java**
- contains methods for actions executed on Basket page 

**Flight.java**
- contains methods for actions executed on Flight page 

**Home.java**
- contains methods for actions executed on Home page 

**Insurance.java**
- contains methods for actions executed on Insurance page 

**Luggage.java**
- contains methods for actions executed on Luggage page 

**Payment.java**
- contains methods for actions executed on Payment page

**Seats.java**
- contains methods for actions executed on Seats page 

**Transport.java**
- contains methods for actions executed on Transport page

### pageObjects ###
Contains locators of every elements we interact with.

**BasketPage.java**
- contains locators of elements we interact with on Basket page 

**FlightPage.java**
- contains locators of elements we interact with on Flight page 

**HomePage.java**
- contains locators of elements we interact with on Home page 

**InsurancePage.java**
- contains locators of elements we interact with on Insurance page 

**LuggagePage.java**
- contains locators of elements we interact with on Luggage page 

**PaymentPage.java**
- contains locators of elements we interact with on Payment page

**SeatsPage.java**
- contains locators of elements we interact with on Seats page 

**TransporPage.java**
- contains locators of elements we interact with on Transport page

### utils ###
Contains logic or functionality needed to run tests with efficiency:

**PropertyFileReader.java** 
- reads test data, file path that are specified in the ".properties" files

**SeleniumUtility.java** 
- contains custom methods for actions with Selenium WebDriver

### stepDefs ###
Contains Step Definitions methods for the automated feature

**StepDef.java** 
- contains Step Definitions for the assessment feature

### testrunner ###
Contains TestRunner.java test class to run the automation.

### resources ###
Contains different resource files used throughout the project

**cucumber.properties** 
- contains configurations to enable publishing of Cucumber Report

**log4j.properties** 
- contains configurations for log4j loggers

**web.properties** 
- contains configurations test data, file path

**logs.out** 
- contains last executed test logs

### features ###
- contains different features used for the Ryanair project

**bookFlightTicket.feature** 
- contains Gherkin scenarios for the Feature

---

## EXECUTING TEST SUITE ##
1. Open TestRunner.class (src/test/java/testrunner/TestRunner.java)
2. Right-click in the class an select Run 'TestRunner'

			OR
			
3. Run test with CMD: $ mvn clean install

---

## GENERATE CUCUMBER REPORT on Mac OS ##

1. Execute a test

2. From the terminal, scroll down to the end of the logs and select the generated report url:

	e.g. │       View your Cucumber Report at:                                            │
         │ 		 https://reports.cucumber.io/reports/e9662018-fa8a-46e5-a291-a4f76484c309 │
		 
		 
		 OR
	
3. Open cucumber.html file from "build/cucumber-report/cucumber.html" with any browser: 
	
---

## IMPORTANT NOTE ##
1. Since the automation is running on a live environment, there might be a chance that some time during execution of the test case, it fails due to the facts that:
	* The selected Departure date is not available because it is in the past
	* There is no more available seats to select from
	
2. Bbecause of that I recorded an successful execution of the test. And the video can be downloaded from this link:
[Successful Cucumber test execution](https://mega.nz/file/SQwi3bhB#Bu4nox-ZvsgfeBoH5vm5w995s7J7UW2lGCGN7yJwYDc)
	 
---


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).