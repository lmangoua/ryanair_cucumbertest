#Author: lionel.mangoua
#Date: 09/09/21

Feature: Book and Pay For Flight Ticket

  Background: User should navigate to Ryanair website, book a flight ticket, pay for booking using an invalid bank card and received a declined payment message
  @negative_test
  Scenario: Book flight ticket using invalid bank car
    Given I make a booking from "DUB" to "BER" on 25/10/2021 for 2 adults and 1 child
    When I pay for booking with card details "5555 5555 5555 5557", "10/23" and "265"
    Then I should get payment declined message