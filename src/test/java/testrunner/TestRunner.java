package testrunner;

/**
 * @author lionel.mangoua
 * date: 09/09/21
 */

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.Test;

@Test
@CucumberOptions(
        plugin = {
                "pretty",
                "json:build/cucumber-report/cucumber.json",
                "html:build/cucumber-report/cucumber.html",
                "junit:build/cucumber-report/cucumber.xml"},
        glue = {"stepDefs", "engine"},
        tags = "@negative_test",
        features = {"src/test/resources/features/bookFlightTicket.feature"})
public class TestRunner extends AbstractTestNGCucumberTests {

    //
}
