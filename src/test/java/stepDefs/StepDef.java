package stepDefs;

import implementation.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.PropertyFileReader;

import static engine.Hook.web_fileName;

public class StepDef {

    PropertyFileReader property = new PropertyFileReader();
    Home home = new Home();
    Flights flights = new Flights();
    Seats seats = new Seats();
    Luggage luggage = new Luggage();
    Insurance insurance = new Insurance();
    Transport transport = new Transport();
    Basket basket = new Basket();
    Payment payment = new Payment();

    String url = property.returnPropVal_web(web_fileName, "url");
    String email = property.returnPropVal_web(web_fileName, "email");
    String password = property.returnPropVal_web(web_fileName, "password");
    String typeOfTrip = property.returnPropVal_web(web_fileName, "typeOfTrip");
    String titleAdult1 = property.returnPropVal_web(web_fileName, "titleAdult1");
    String firstNameAdult1 = property.returnPropVal_web(web_fileName, "firstNameAdult1");
    String lastNameAdult1 = property.returnPropVal_web(web_fileName, "lastNameAdult1");
    String titleAdult2 = property.returnPropVal_web(web_fileName, "titleAdult2");
    String firstNameAdult2 = property.returnPropVal_web(web_fileName, "firstNameAdult2");
    String lastNameAdult2 = property.returnPropVal_web(web_fileName, "lastNameAdult2");
    String titleChild1 = property.returnPropVal_web(web_fileName, "titleChild1");
    String firstNameChild1 = property.returnPropVal_web(web_fileName, "firstNameChild1");
    String lastNameChild1 = property.returnPropVal_web(web_fileName, "lastNameChild1");
    String phoneNumber = property.returnPropVal_web(web_fileName, "phoneNumber");
    String addressLine1 = property.returnPropVal_web(web_fileName, "addressLine1");
    String addressLine2 = property.returnPropVal_web(web_fileName, "addressLine2");
    String city = property.returnPropVal_web(web_fileName, "city");
    String country = property.returnPropVal_web(web_fileName, "country");
    String cardHolderName = property.returnPropVal_web(web_fileName, "cardHolderName");
    String currencyValue = property.returnPropVal_web(web_fileName, "currencyValue");
    String zipCode = property.returnPropVal_web(web_fileName, "zipCode");

    @Given("I make a booking from {string} to {string} on {int}\\/{int}\\/{int} for {int} adults and {int} child")
    public void i_make_a_booking_from_dub_to_on_for_adults_and_child(String departureCity, String destinationCity, int day, int month,
                                                                     int year, int numberOfAdult, int numberOfChildren) {

        home.navigateToUrl(url);

        home.closePrivacyPopup();

        home.selectTypeOfTrip(typeOfTrip);

        home.selectDepartureCity(departureCity);

        home.selectDestinationCity(destinationCity);

        home.enterTripDate(day + "", month + "", year + "");

        home.selectNumberOfPassengers(numberOfAdult, numberOfChildren);

        home.searchFlights();

        flights.selectFlight(day + "", email, password, titleAdult1, titleAdult2, titleChild1,
                firstNameAdult1, firstNameAdult2, firstNameChild1, lastNameAdult1, lastNameAdult2, lastNameChild1);

        seats.selectSeats(numberOfAdult, numberOfChildren);

        luggage.selectLuggage();

        insurance.selectInsurance();

        transport.selectTransport();

        basket.viewBasketAndCheckout();
    }

    @When("I pay for booking with card details {string}, {string} and {string}")
    public void i_pay_for_booking_with_card_details_and(String cardNumber, String expiryDate, String cvvNumber) {

        payment.contactDetails(phoneNumber);

        payment.selectTravelInsurance();

        payment.cardDetails(cardNumber, expiryDate, cvvNumber, cardHolderName);

        payment.billingAddress(addressLine1, addressLine2, city, country, zipCode);

        payment.payNow(currencyValue);
    }

    @Then("I should get payment declined message")
    public void i_should_get_payment_declined_message() {

        payment.validatePaymentErrorMessage();
    }
}
