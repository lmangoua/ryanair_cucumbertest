package implementation;

/**
 * @author lionel.mangoua
 * date: 11/09/21
 */

import engine.Hook;
import pageObjects.LuggagePage;
import utils.SeleniumUtility;

public class Luggage extends Hook {

    //region <selectLuggage>
    public static void selectLuggage() {

        //validate Luggage page
        SeleniumUtility.waitForElementToBeLocatedByXpath(LuggagePage.needToCheckInAnyBagsTitleLabelXpath(), 3, "Failed to wait for 'Family seating' title");

        //click '1 small Bag only' radio button
        SeleniumUtility.clickElementByXpath(LuggagePage.oneSmallBagOnlyLabelXpath(), "Failed to click '1 small Bag only' radio button");

        //scroll to 'Continue' button and click
        SeleniumUtility.scrollToElementByXpath(LuggagePage.continueButtonXpath(), "Failed to scroll to 'Continue' button");
        SeleniumUtility.clickElementByXpath(LuggagePage.continueButtonXpath(), "Failed to click 'Continue' button");

        log("Selected 'Luggage' successfully\n","INFO",  "text");
    }
    //endregion
}
