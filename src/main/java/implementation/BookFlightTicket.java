package implementation;

/**
 * @author lionel.mangoua
 * date: 09/09/21
 */

import engine.Hook;
import utils.SeleniumUtility;

public class BookFlightTicket extends Hook {

    //region <navigateToUrl>
    public static void navigateToUrl(String url) {

        driver.get(url);

//        SeleniumUtility.waitForElementByXpath(RyanairHomePage.amazonLogoIconXpath(), "Failed to wait for 'Amazon' logo");

//        log("Navigated to " + url + " successfully","INFO",  "text");
        System.out.println("Navigated to " + url + " successfully \n");
    }
    //endregion
}
