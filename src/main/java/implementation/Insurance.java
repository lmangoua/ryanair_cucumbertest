package implementation;

/**
 * @author lionel.mangoua
 * date: 12/09/21
 */

import engine.Hook;
import pageObjects.InsurancePage;
import pageObjects.LuggagePage;
import utils.SeleniumUtility;

public class Insurance extends Hook {

    //region <selectInsurance>
    public static void selectInsurance() {

        //validate Insurance page
        SeleniumUtility.waitForElementToBeLocatedByXpath(InsurancePage.theBestDealForYourTripTitleLabelXpath(), 3, "Failed to wait for 'The best deals for your trip' title");

        //scroll to 'Continue' button and click
        SeleniumUtility.scrollToElementByXpath(LuggagePage.continueButtonXpath(), "Failed to scroll to 'Continue' button");
        SeleniumUtility.clickElementByXpath(LuggagePage.continueButtonXpath(), "Failed to click 'Continue' button");

        log("Selected 'Insurance' successfully\n","INFO",  "text");
    }
    //endregion
}
