package implementation;

/**
 * @author lionel.mangoua
 * date: 11/09/21
 */

import engine.Hook;
import pageObjects.FlightsPage;
import utils.SeleniumUtility;

public class Flights extends Hook {

    //region <selectFlight>
    public static void selectFlight(String day, String email, String pwd, String titleAdult1, String titleAdult2,
                                    String titleChild1, String fNameAdult1, String fNameAdult2, String fNameChild1,
                                    String lNameAdult1, String lNameAdult2, String lNameChild1) {

        //select Flight no
        flight(day);

        //select Fares
        fares();

        //validate 'selected flight' & 'selected fare'
        validateSelectedFlightAndFare();

        //login to Ryanair account
        loginToRyanair(email, pwd);

        //enter Passengers Details
        enterPassengersDetails(titleAdult1, titleAdult2, titleChild1, fNameAdult1, fNameAdult2, fNameChild1,
                lNameAdult1, lNameAdult2, lNameChild1);

        log("Selected flight successfully\n","INFO",  "text");
    }
    //endregion

    //region <flight>
    public static void flight(String day) {

        //validate Flights page
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.overviewTabXpath(), 3, "Failed to wait for 'Overview' tab");

        //click Flight day tab
        SeleniumUtility.clickElementByXpath(FlightsPage.selectDepartureFlightsDayTabXpath(day), "Failed to click 'Flight day " + day + "' tab");

        //extract Flight no


        //click Flight Number
        SeleniumUtility.clickElementByXpath(FlightsPage.flightNumberLabelXpath(), "Failed to click 'Flight no.' label");

        log("Selected Flight no '" + extractedFlightNo + "' successfully\n","INFO",  "text");
    }
    //endregion

    //region <fares>
    public static void fares() {

        //validate Fares Title
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.faresTitleXpath(), 3, "Failed to wait for 'Fares' title");

        //click 'Values' fares'
        SeleniumUtility.clickElementByXpath(FlightsPage.valuesTitleXpath(), "Failed to click 'Values' title");

        //click 'Continue with Value fare' button
        SeleniumUtility.waitForElementToBeClickableByXpath(FlightsPage.continueWithValueFareButtonXpath(), 3, "Failed to wait for 'Continue with Value fare' button");
        SeleniumUtility.clickElementByXpath(FlightsPage.continueWithValueFareButtonXpath(), "Failed to click 'Continue with Value fare' button");
        SeleniumUtility.pause(2000);

        log("Selected 'Fares' successfully\n","INFO",  "text");
    }
    //endregion

    //region <validateSelectedFlightAndFare>
    public static void validateSelectedFlightAndFare() {

        //validate 'Your selected flight' Title
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.yourSelectedFlightTitleXpath(), 3, "Failed to wait for 'Your selected flight' Title");

        //validate 'Your selected fare' Title
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.yourSelectedFareTitleXpath(), 3, "Failed to wait for 'Your selected fare' Title");

        log("Validated 'Selected Flight & Fares' successfully\n","INFO",  "text");
    }
    //endregion

    //region <loginToRyanair>
    public static void loginToRyanair(String email, String password) {

        //click login button
        SeleniumUtility.clickElementByXpath(FlightsPage.loginButtonXpath(), "Failed to click 'Log in' button");

        //enter email
        SeleniumUtility.clearTextAndEnterValueByXpath(FlightsPage.emailTextboxXpath(), email, "Failed to enter 'email'");

        //enter password
        SeleniumUtility.clearTextAndEnterValueByXpath(FlightsPage.passwordTextboxXpath(), password, "Failed to enter 'password'");

        //click login button
        SeleniumUtility.clickElementByXpath(FlightsPage.log_inButtonXpath(), "Failed to click 'Log in' button");

        log("Logged in to 'Ryanair' successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterPassengersDetails>
    public static void enterPassengersDetails(String titleAdult1, String titleAdult2,
                                              String titleChild1, String fNameAdult1, String fNameAdult2, String fNameChild1,
                                              String lNameAdult1, String lNameAdult2, String lNameChild1) {

        //validate 'Your selected flight' Title
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.pleaseEnterNamesAsTheyAppearLabelXpath(), 3, "Failed to wait for 'Please enter names as they appear on passport or travel documentation' label");

        //adult passenger 1
        enterAdultPassenger1Details(titleAdult1, fNameAdult1, lNameAdult1);

        //adult passenger 2
        enterAdultPassenger2Details(titleAdult2, fNameAdult2, lNameAdult2);

        //child passenger 1
        enterChildPassenger1Details(titleChild1, fNameChild1, lNameChild1);

        //click 'Continue' button
        SeleniumUtility.clickElementByXpath(FlightsPage.continueButtonXpath(), "Failed to click 'Continue' button");

        log("Entered 'Passengers' details successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterAdultPassenger1Details>
    public static void enterAdultPassenger1Details(String title, String fName, String lName) {

        //select Title
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.needSpecialAssistanceLabel1Xpath(), 4, "Failed to wait for 'Need special assistance?' label");

        SeleniumUtility.pause(2000);
        SeleniumUtility.clickElementByXpath(FlightsPage.title1DropdownListXpath(), "Failed to click 'Title' dropdown list");
        SeleniumUtility.clickElementByXpath(FlightsPage.title1LabelXpath(title), "Failed to select '" + title + "' from dropdown list");

        //enter First name
        SeleniumUtility.enterTextByXpath(FlightsPage.adultPass1FirstnameTextboxXpath(), fName, "Failed to enter '" + fName + "'");

        //enter Last name
        SeleniumUtility.enterTextByXpath(FlightsPage.adultPass1LastnameTextboxXpath(), lName, "Failed to enter '" + lName + "'");
        SeleniumUtility.scrollToElementByXpath(FlightsPage.needSpecialAssistanceLabel1Xpath(), "Failed to scroll to 'Need special assistance?' label");

        log("Entered 'Adult Passenger 1' details successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterAdultPassenger2Details>
    public static void enterAdultPassenger2Details(String title, String fName, String lName) {

        //select Title
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.needSpecialAssistanceLabel2Xpath(), 3, "Failed to wait for 'Need special assistance?' label");
        SeleniumUtility.clickElementByXpath(FlightsPage.title1DropdownListXpath(), "Failed to click 'Title' dropdown list");
        SeleniumUtility.clickElementByXpath(FlightsPage.title1LabelXpath(title), "Failed to select '" + title + "' from dropdown list");

        //enter First name
        SeleniumUtility.enterTextByXpath(FlightsPage.adultPass2FirstnameTextboxXpath(), fName, "Failed to enter '" + fName + "'");

        //enter Last name
        SeleniumUtility.enterTextByXpath(FlightsPage.adultPass2LastnameTextboxXpath(), lName, "Failed to enter '" + lName + "'");
        SeleniumUtility.scrollToElementByXpath(FlightsPage.needSpecialAssistanceLabel1Xpath(), "Failed to scroll to 'Need special assistance?' label");

        log("Entered 'Adult Passenger 2' details successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterChildPassenger1Details>
    public static void enterChildPassenger1Details(String title, String fName, String lName) {

        //enter First name
        SeleniumUtility.enterTextByXpath(FlightsPage.childPass1FirstnameTextboxXpath(), fName, "Failed to enter '" + fName + "'");

        //enter Last name
        SeleniumUtility.enterTextByXpath(FlightsPage.childPass1LastnameTextboxXpath(), lName, "Failed to enter '" + lName + "'");

        log("Entered 'Child Passenger 1' details successfully\n","INFO",  "text");
    }
    //endregion
}
