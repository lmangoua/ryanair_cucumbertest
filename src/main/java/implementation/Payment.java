package implementation;

/**
 * @author lionel.mangoua
 * date: 12/09/21
 */

import engine.Hook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageObjects.BasketPage;
import pageObjects.HomePage;
import pageObjects.LuggagePage;
import pageObjects.PaymentPage;
import utils.SeleniumUtility;

import java.util.List;

public class Payment extends Hook {

    //region <contactDetails>
    public static void contactDetails(String phoneNumber) {

        //validate 'Payment' page
        SeleniumUtility.waitForElementToBeLocatedByXpath(PaymentPage.contactDetailsTitleXpath(), 3, "Failed to wait for 'Contact details' title");

        //enter phone number
        SeleniumUtility.enterTextByXpath(PaymentPage.phoneNumberTextboxXpath(), phoneNumber, "Failed to enter '" + phoneNumber + "'");

        log("Entered 'Contact Details' successfully\n","INFO",  "text");
    }
    //endregion

    //region <selectTravelInsurance>
    public static void selectTravelInsurance() {

        //click 'I don't want to be insured' radio button
        SeleniumUtility.clickElementByXpath(PaymentPage.iDontWantInsuranceRadButtonXpath(), "Failed to click 'I don't want to be insured' radio button");

        log("Selected 'I don't want to be insured' successfully\n","INFO",  "text");
    }
    //endregion

    //region <cardDetails>
    public static void cardDetails(String cardNumber, String expiryDate, String cvvNumber, String cardHolderName) {

        //scroll to 'Pay now' button and click
        SeleniumUtility.scrollToElementByXpath(PaymentPage.payNowButtonXpath(), "Failed to scroll to 'Pay now' button");

        //enter 'Card number'
        SeleniumUtility.switchToFrameByIndex(0); //switch to frame
        SeleniumUtility.enterTextByXpath(PaymentPage.cardNumberTextboxXpath(), "5555 5555 5555 5557", "Failed to enter '5555 5555 5555 5557'");
        SeleniumUtility.switchToDefaultContent();

        //enter 'Expiry date'
        SeleniumUtility.enterTextByXpath(PaymentPage.expiryDateTextboxXpath(), expiryDate, "Failed to enter '" + expiryDate + "'");

        //enter 'Security code'
        SeleniumUtility.enterTextByXpath(PaymentPage.securityCodeTextboxXpath(), cvvNumber, "Failed to enter '" + cvvNumber + "'");

        //enter 'Cardholder name'
        SeleniumUtility.enterTextByXpath(PaymentPage.cardHolderNameTextboxXpath(), cardHolderName, "Failed to enter '" + cardHolderName + "'");

        log("Entered 'Card Details' successfully\n","INFO",  "text");
    }
    //endregion

    //region <billingAddress>
    public static void billingAddress(String addressLine1, String addressLine2, String city, String country, String zipCode) {

        //enter 'Address line 1'
        SeleniumUtility.enterTextByXpath(PaymentPage.addressLine1TextboxXpath(), addressLine1, "Failed to enter '" + addressLine1 + "'");

        //enter 'Address line 2'
        SeleniumUtility.enterTextByXpath(PaymentPage.addressLine2TextboxXpath(), addressLine2, "Failed to enter '" + addressLine2 + "'");

        //enter 'City'
        SeleniumUtility.enterTextByXpath(PaymentPage.cityTextboxXpath(), city, "Failed to enter '" + city + "'");

        //enter 'Country'
        SeleniumUtility.enterTextByXpath(PaymentPage.countryTextboxXpath(), country, "Failed to enter '" + country + "'");
        SeleniumUtility.sendKeys("ENTER");

        //enter 'Zip code'
        SeleniumUtility.enterTextByXpath(PaymentPage.zipCodeTextboxXpath(), zipCode, "Failed to enter '" + zipCode + "'");

        //enter 'Country'
        SeleniumUtility.clearTextByXpath(PaymentPage.countryTextboxXpath(), "Failed to clear 'Country'");
        SeleniumUtility.enterTextByXpath(PaymentPage.countryTextboxXpath(), country, "Failed to enter '" + country + "'");
//        SeleniumUtility.clearTextAndEnterValueByXpath(PaymentPage.countryTextboxXpath(), country, "Failed to enter '" + country + "'");
//        SeleniumUtility.clickElementByXpath(PaymentPage.country_LabelXpath(country), "Failed to clear 'Country' label");
        SeleniumUtility.clickElementByXpath(PaymentPage.cityLabelXpath(), "Failed to clear 'City' label");

        log("Entered 'Billing Address' successfully\n","INFO",  "text");
    }
    //endregion

    //region <payNow>
    public static void payNow(String currency) {

        //select currency
        SeleniumUtility.clickElementByXpath(PaymentPage.currencyButtonXpath(), "Failed to click 'Currency' button");
        SeleniumUtility.clickElementByXpath(PaymentPage.currencyLabelXpath(currency), "Failed to click '" + currency + "' value");

        //click 'By clicking Pay Now ..' checkbox
        SeleniumUtility.clickElementByXpath(PaymentPage.termAndConditionsCheckboxXpath(), "Failed to click By clicking Pay Now ..' checkbox");

        //click 'Pay now' button
        SeleniumUtility.clickElementByXpath(PaymentPage.payNowButtonXpath(), "Failed to click 'Pay now' button");

        log("Entered 'Contact Details' successfully\n","INFO",  "text");
    }
    //endregion

    //region <validatePaymentErrorMessage>
    public static void validatePaymentErrorMessage() {

        //validate 'Transaction could not be processed. Your payment was not authorised ...' error message
        SeleniumUtility.waitForElementToBeLocatedByXpath(PaymentPage.transactionCouldNotBeProcessedErrorMessageLabelXpath(), 3, "Failed to wait for 'Transaction could not be processed. Your payment was not authorised ...' error message");

        log("Validated payment error message 'Oops, something went wrong. Please check your payment details and try again' successfully\n","INFO",  "text");
    }
    //endregion
}