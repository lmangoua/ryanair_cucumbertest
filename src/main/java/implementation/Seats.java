package implementation;

/**
 * @author lionel.mangoua
 * date: 11/09/21
 */

import engine.Hook;
import org.openqa.selenium.By;
import pageObjects.FlightsPage;
import pageObjects.HomePage;
import pageObjects.SeatsPage;
import utils.SeleniumUtility;

public class Seats extends Hook {

    //region <selectSeats>
    public static void selectSeats(int numberOfAdult, int numberOfChildren) {

        //validate Seats page
        SeleniumUtility.waitForElementToBeLocatedByXpath(SeatsPage.familySeatingTitleLabelXpath(), 3, "Failed to wait for 'Family seating' title");

        //click 'Okay, got it.' button
        SeleniumUtility.clickElementByXpath(SeatsPage.okGotItButtonXpath(), "Failed to click 'Okay, got it.' button");

        int totalNumberOfPassengers = numberOfAdult + numberOfChildren;

        char[] seatsCharArray = {'A', 'B', 'C', 'D', 'E', 'F'};
        int i = 0;
        int m = 1;
        while (i < totalNumberOfPassengers) {
            log("Increment #" + m,"INFO",  "text");

            /* check if seat 1 is free, if Yes, select; if No, move to next available seat */
            //select seat Passenger 1
            SeleniumUtility.getAttributeBy(By.xpath("//button[@id='seat-23A']"), "id", "Failed to get id attribute");

            SeleniumUtility.clickElementByXpath(SeatsPage.selectAvailableSeatButtonXpath("23", String.valueOf(seatsCharArray[i])), "Failed to click Seat '23" + seatsCharArray[i] + "' button");

//            for(i = 0; i < seatsCharArray.length; i++) {
//                System.out.println(seatsCharArray[i]);
//
//                //click seat
//                SeleniumUtility.clickElementByXpath(SeatsPage.selectAvailableSeatButtonXpath("19", String.valueOf(seatsCharArray[0])), "Failed to click Seat '19" + seatsCharArray[0] + "' button");
//            }



//            //click button to increase number + 1
//            SeleniumUtility.clickElementByXpath(HomePage.increaseChildrenButtonXpath(), "Failed to click 'Increase Children' button");
//
//            String extractedNumberOfChildren;
//            extractedNumberOfChildren = SeleniumUtility.extractTextByXpath(HomePage.defaultNumberOfChildrenLabelXpath(), "Failed to extract 'updated number of Children'");

//            log("extractedNumberOfChildren: " + extractedNumberOfChildren + "'","INFO",  "text");
            i++;
            m++;
        }

        //select seat Passenger 2

        //select seat Passenger 3

        //click 'Continue' button
        SeleniumUtility.clickElementByXpath(SeatsPage.continueButtonXpath(), "Failed to click 'Continue' button");

        //click 'No, thanks' link
        SeleniumUtility.clickElementByXpath(SeatsPage.noThanksButtonXpath(), "Failed to click 'No, thanks' button");

        log("Selected 'Seats' successfully\n","INFO",  "text");
    }
    //endregion
}
