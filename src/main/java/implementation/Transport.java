package implementation;

/**
 * @author lionel.mangoua
 * date: 11/09/21
 */

import engine.Hook;
import pageObjects.TransportPage;
import utils.SeleniumUtility;

public class Transport extends Hook {

    //region <selectTransport>
    public static void selectTransport() {

        //validate Luggage page
        SeleniumUtility.waitForElementToBeLocatedByXpath(TransportPage.benefitsOfParkingTitleLabelXpath(), 3, "Failed to wait for 'Benefits of Parking:' title");

        //scroll to 'Continue' button and click
        SeleniumUtility.scrollToElementByXpath(TransportPage.continueButtonXpath(), "Failed to scroll to 'Continue' button");
        SeleniumUtility.clickElementByXpath(TransportPage.continueButtonXpath(), "Failed to click 'Continue' button");

        log("Selected 'Transport' successfully\n","INFO",  "text");
    }
    //endregion
}
