package implementation;

/**
 * @author lionel.mangoua
 * date: 12/09/21
 */

import engine.Hook;
import pageObjects.BasketPage;
import pageObjects.PaymentPage;
import pageObjects.TransportPage;
import utils.SeleniumUtility;

public class Basket extends Hook {

    //region <viewBasketAndCheckout>
    public static void viewBasketAndCheckout() {

        //click 'View basket' link
        SeleniumUtility.clickElementByXpath(BasketPage.viewBasketLinkXpath(), "Failed to click '1 small Bag only' radio button");

        //validate 'Check out' popup page
        SeleniumUtility.waitForElementToBeLocatedByXpath(BasketPage.checkoutButtonXpath(), 3, "Failed to wait for 'Check out' button");

        //click 'Check out' button
        SeleniumUtility.clickElementByXpath(BasketPage.checkoutButtonXpath(), "Failed to click 'Check out' button");

        //validate 'Payment' page
        SeleniumUtility.waitForElementToBeLocatedByXpath(PaymentPage.contactDetailsTitleXpath(), 3, "Failed to wait for 'Contact details' title");

        log("Selected 'View basket and Check out' successfully\n","INFO",  "text");
    }
    //endregion
}
