package implementation;

/**
 * @author lionel.mangoua
 * date: 09/09/21
 */

import engine.Hook;
import pageObjects.FlightsPage;
import pageObjects.HomePage;
import utils.SeleniumUtility;

public class Home extends Hook {

    //region <navigateToUrl>
    public static void navigateToUrl(String url) {

        driver.get(url);

        log("Navigated to " + url + " successfully\n","INFO",  "text");
    }
    //endregion

    //region <closePrivacyPopup>
    public static void closePrivacyPopup() {

        //validate Home page
        SeleniumUtility.waitForElementToBeLocatedByXpath(HomePage.weValueYourPrivacyLabelXpath(), 3, "Failed to wait for 'We value your privacy' popup");

        //close popup
        SeleniumUtility.clickElementByXpath(HomePage.yesAgreeButtonXpath(), "Failed to click 'Yes, I agree' button");

        log("Closed popup successfully\n","INFO",  "text");
    }
    //endregion

    //region <selectTypeOfTrip>
    public static void selectTypeOfTrip(String typeOfTrip) {

        //click 'Flights' tab
        SeleniumUtility.clickElementByXpath(HomePage.flightsButtonXpath(), "Failed to click 'Flights' tab");

        //click trip type
        SeleniumUtility.clickElementByXpath(HomePage.tripTypeRadioButtonXpath(typeOfTrip), "Failed to click '" + typeOfTrip + "' radio button");

        log("Selected type of trip '" + typeOfTrip + "' successfully\n","INFO",  "text");
    }
    //endregion

    //region <selectDepartureCity>
    public static void selectDepartureCity(String departureCity) {

        //enter departure city
        SeleniumUtility.clearTextAndEnterValueByXpath(HomePage.departureTextboxXpath(), departureCity, "Failed to enter '" + departureCity + "'");

        log("Selected departure city '" + departureCity + "' successfully\n","INFO",  "text");
    }
    //endregion

    //region <selectDestinationCity>
    public static void selectDestinationCity(String destinationCity) {

        //enter destination city
        SeleniumUtility.clearTextAndEnterValueByXpath(HomePage.destinationTextboxXpath(), destinationCity, "Failed to enter '" + destinationCity + "'");

        //select Airport
        SeleniumUtility.clickElementByXpath(HomePage.berAirportLabelXpath(), "Failed to click 'Berlin Brandenburg' label");

        log("Selected destination city '" + destinationCity + "' successfully\n","INFO",  "text");
    }
    //endregion

    //region <enterTripDate>
    public static void enterTripDate(String day, String month, String year) {

        //click 'Depart Choose date'
        SeleniumUtility.clickElementByXpath(HomePage.departChooseDateTextboxXpath(), "Failed to click 'Depart Choose date' textbox");

        //select 'Exact dates' tab
        SeleniumUtility.clickElementByXpath(HomePage.exactDatesTabXpath(), "Failed to click 'Exact dates' tab");

        //select Departure date
        SeleniumUtility.clickElementByXpath(HomePage.choseDepartureDateXpath(day, month, year), "Failed to select date '" + day + "/" + month + "/" + year + "'");

        log("Selected Departure date '" + day + "/" + month + "/" + year + "' successfully\n","INFO",  "text");
    }
    //endregion

    //region <selectNumberOfPassengers>
    public static void selectNumberOfPassengers(int numberOfAdults, int numberOfChildren) {

        //click 'Passengers' textbox
        SeleniumUtility.clickElementByXpath(HomePage.passengersTextboxXpath(), "Failed to click 'Passengers' textbox");

        //increase number of Adults
        selectNumberOfAdults(numberOfAdults);

        //increase number of Children
        selectNumberOfChildren(numberOfChildren);

        //validate we selected the right number of passengers
        SeleniumUtility.waitForElementToBeLocatedByXpath(HomePage.numberOfSelectedPassengersLabelXpath(numberOfAdults + "", numberOfChildren + ""), 3, "Failed to wait for '" + numberOfAdults + " Adults, " + numberOfChildren + " Child' label");

        log("Selected '" + numberOfAdults + "' Adults and '" + numberOfChildren + "' Child successfully\n","INFO",  "text");
    }
    //endregion

    //region <selectNumberOfAdults>
    public static void selectNumberOfAdults(int numberOfAdults) {

        //extract default number of Adults
        String extractedDefaultNumberOfAdults;
        extractedDefaultNumberOfAdults = SeleniumUtility.extractTextByXpath(HomePage.defaultNumberOfAdultsLabelXpath(), "Failed to extract 'default number of Adults'");

        int n = 1;
        int i = Integer.parseInt(extractedDefaultNumberOfAdults);
        while (i < numberOfAdults) {
            log("Increment #" + n,"INFO",  "text");

            //click button to increase number + 1
            SeleniumUtility.clickElementByXpath(HomePage.increaseAdultsButtonXpath(), "Failed to click 'Increase Adults' button");

            String extractedNumberOfAdults;
            extractedNumberOfAdults = SeleniumUtility.extractTextByXpath(HomePage.defaultNumberOfAdultsLabelXpath(), "Failed to extract 'updated number of Adults'");

            log("extractedNumberOfAdults: " + extractedNumberOfAdults + "'","INFO",  "text");
            i++;
        }
    }
    //endregion

    //region <selectNumberOfChildren>
    public static void selectNumberOfChildren(int numberOfChildren) {

        //extract default number of Children
        String extractedDefaultNumberOfChildren;
        extractedDefaultNumberOfChildren = SeleniumUtility.extractTextByXpath(HomePage.defaultNumberOfChildrenLabelXpath(), "Failed to extract 'default number of Children'");

        int m = 1;
        int j = Integer.parseInt(extractedDefaultNumberOfChildren);
        while (j < numberOfChildren) {
            log("Increment #" + m,"INFO",  "text");

            //click button to increase number + 1
            SeleniumUtility.clickElementByXpath(HomePage.increaseChildrenButtonXpath(), "Failed to click 'Increase Children' button");

            String extractedNumberOfChildren;
            extractedNumberOfChildren = SeleniumUtility.extractTextByXpath(HomePage.defaultNumberOfChildrenLabelXpath(), "Failed to extract 'updated number of Children'");

            log("extractedNumberOfChildren: " + extractedNumberOfChildren + "'","INFO",  "text");
            j++;
            m++;
        }
    }
    //endregion

    //region <searchFlights>
    public static void searchFlights() {

        //click 'Search' button
        SeleniumUtility.clickElementByXpath(HomePage.searchButtonXpath(), "Failed to click 'Search' button");

        //validate next page
        SeleniumUtility.waitForElementToBeLocatedByXpath(FlightsPage.overviewTabXpath(), 3, "Failed to wait for 'Overview' tab");

        log("Clicked 'Search' button successfully\n","INFO",  "text");
    }
    //endregion
}
