package pageObjects;

public class InsurancePage {

    //*********** By xpath ***********
    public static String theBestDealForYourTripTitleLabelXpath() {
        return "//h3[contains(text(), 'The best deals for your trip')]";
    }

    public static String continueButtonXpath() {
        return "//button[contains(text(), ' Continue')]";
    }
}


