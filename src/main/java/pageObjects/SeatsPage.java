package pageObjects;

import org.openqa.selenium.By;

public class SeatsPage {

    //*********** By xpath ***********
    public static String familySeatingTitleLabelXpath() {
        return "//h4[contains(text(), 'Family seating')]";
    }

    public static String okGotItButtonXpath() {
        return "//button[contains(text(), ' Okay, got it. ')]";
    }

    public static String selectAvailableSeatButtonXpath(String row, String letter) {
        return "//button[@id='seat-" + row + "" + letter + "']"; //button[@id='seat-19B']
    }

    public static String continueButtonXpath() {
        return "//button[contains(text(), ' Continue ')]";
    }

    public static String noThanksButtonXpath() {
        return "//button[contains(text(), ' No, thanks ')]";
    }

    public static String selectAvailableSeat1Xpath(String value) {
        return "//div[@class='seatmap__seatrow seatmap__seatrow--19 ng-star-inserted']/div/button[@id='seat-19A']";
    } //div[@class='seatmap__seatrow seatmap__seatrow--20 ng-star-inserted']/div/button[@id='seat-20B']
} //div[@id='seat-20B']  ==> unavailable is a div, //button[@id='seat-19A']  ==> available is a button


