package pageObjects;

public class HomePage {

    //*********** By id ***********

    //*********** By xpath ***********
    public static String weValueYourPrivacyLabelXpath() {
        return "//p[contains(text(), 'We value your privacy')]";
    }

    public static String yesAgreeButtonXpath() {
        return "//button[contains(text(), 'Yes, I agree')]";
    }

    public static String flightsButtonXpath() {
        return "//button[@aria-label='flights']";
    }

    public static String tripTypeRadioButtonXpath(String value) {
        return "//label[contains(text(), '" + value + "')]";
    }

    public static String departureTextboxXpath() {
        return "//input[@id='input-button__departure']";
    }

    public static String destinationTextboxXpath() {
        return "//input[@id='input-button__destination']";
    }

    public static String berAirportLabelXpath() {
        return "//span[contains(text(), ' Berlin Brandenburg ')]";
    }

    public static String departChooseDateTextboxXpath() {
        return "//div[@data-ref='input-button__dates-from']";
    }

    public static String exactDatesTabXpath() {
        return "//span[contains(text(), 'Exact dates')]";
    }

    public static String choseDepartureDateXpath(String day, String month, String year) {
        return "//div[@data-id='" + year + "-" + month + "-" + day + "']";
    }

    public static String passengersTextboxXpath() {
        return "//div[contains(text(), ' Passengers ')]";
    }

    public static String defaultNumberOfAdultsLabelXpath() {
        return "//ry-counter[@data-ref='passengers-picker__adults']/div/div[@class='counter__value']";
    }

    public static String defaultNumberOfChildrenLabelXpath() {
        return "//ry-counter[@data-ref='passengers-picker__children']/div/div[@class='counter__value']";
    }

    public static String increaseAdultsButtonXpath() {
        return "//ry-counter[@data-ref='passengers-picker__adults']/div/div[@class='counter__button-wrapper--enabled']";
    }

    public static String increaseChildrenButtonXpath() {
        return "//ry-counter[@data-ref='passengers-picker__children']/div/div[@class='counter__button-wrapper--enabled']";
    }

    public static String numberOfSelectedPassengersLabelXpath(String numOfAdults, String numOfChildren) {
        return "//div[contains(text(), ' " + numOfAdults + " Adults, " + numOfChildren + " Child ')]";
    }

    public static String searchButtonXpath() {
        return "//button[@data-ref='flight-search-widget__cta']";
    }
}
