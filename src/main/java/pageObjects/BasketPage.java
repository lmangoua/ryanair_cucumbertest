package pageObjects;

public class BasketPage {

    //*********** By xpath ***********
    public static String viewBasketLinkXpath() {
        return "//a[contains(text(), ' View basket ')]";
    }

    public static String benefitsOfParkingTitleLabelXpath() {
        return "//p[contains(text(), ' Benefits of Parking:')]";
    }

    public static String checkoutButtonXpath() {
        return "//button[contains(text(), ' Check out ')]";
    }

    public static String continueButtonXpath() {
        return "//button[contains(text(), ' Continue')]";
    }
}


