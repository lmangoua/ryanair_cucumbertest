package pageObjects;

public class TransportPage {

    //*********** By xpath ***********
    public static String benefitsOfParkingTitleLabelXpath() {
        return "//p[contains(text(), ' Benefits of Parking:')]";
    }

    public static String continueButtonXpath() {
        return "//button[contains(text(), ' Continue')]";
    }
}


