package pageObjects;

public class LuggagePage {

    //*********** By xpath ***********
    public static String needToCheckInAnyBagsTitleLabelXpath() {
        return "//span[contains(text(), ' Need to check in any bags? ')]";
    }

    public static String oneSmallBagOnlyLabelXpath() {
        return "//p[contains(text(), '1 Small Bag only')]/../../..//ry-radio-circle-button";
    }

    public static String continueButtonXpath() {
        return "//button[contains(text(), ' Continue')]";
    }
}


