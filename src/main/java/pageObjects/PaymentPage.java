package pageObjects;

public class PaymentPage {

    //*********** By xpath ***********
    public static String contactDetailsTitleXpath() {
        return "//h3[contains(text(), ' Contact details ')]";
    }

    public static String phoneNumberTextboxXpath() {
        return "//label[contains(text(), 'Phone number *')]/../div/input";
    }

    public static String cardNumberTextboxXpath() {
        return "//label[contains(text(), 'Card number *')]/../div/input";
    }

    public static String expiryDateTextboxXpath() {
        return "//label[contains(text(), 'Expiry date *')]/../div/input";
    }

    public static String securityCodeTextboxXpath() {
        return "//label[contains(text(), 'Security code *')]/../div/input";
    }

    public static String cardHolderNameTextboxXpath() {
        return "//label[contains(text(), 'Cardholder name *')]/../div/input";
    }

    public static String payNowButtonXpath() {
        return "//button[contains(text(), ' Pay now ')]";
    }

    public static String addressLine1TextboxXpath() {
        return "//label[contains(text(), 'Address line 1 *')]/../div/input";
    }

    public static String addressLine2TextboxXpath() {
        return "//label[contains(text(), 'Address line 2')]/../div/input";
    }

    public static String cityTextboxXpath() {
        return "//label[contains(text(), 'City *')]/../div/input";
    }

    public static String countryTextboxXpath() {
        return "//label[contains(text(), 'Country *')]/../../input";
    }

    public static String cityLabelXpath() {
        return "//label[contains(text(), 'City *')]";
    }

    public static String country_LabelXpath(String value) {
        return "//div[contains(text(), '" + value + "')]";
    }

    public static String termAndConditionsCheckboxXpath() {
        return "//label[@for='termsAndConditions']/div/div[@class='_background']";
    }

    public static String iDontWantInsuranceRadButtonXpath() {
        return "//input[@id='insurance-opt-out']/..";
    }

    public static String paymentErrorMessageLabelXpath() {
        return "//div[contains(text(), 'Oops, something went wrong. Please check your payment details and try again')]";
    }

    public static String transactionCouldNotBeProcessedErrorMessageLabelXpath() {
        return "//div[contains(text(), 'Transaction could not be processed. Your payment was not authorised therefore we could not complete your booking. Please ensure that the information was correct and try again or use a new payment card.')]";
    }

    public static String currencyButtonXpath() {
        return "//button[@class='dropdown__toggle b2']";
    }

    public static String currencyLabelXpath(String value) {
        return "//div[contains(text(), '" + value + "')]";
    }

    public static String zipCodeTextboxXpath() {
        return "//label[contains(text(), 'Postcode/ZIP code *')]/../div/input";
    }
}


