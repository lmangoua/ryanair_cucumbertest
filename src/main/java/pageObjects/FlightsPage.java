package pageObjects;

import org.openqa.selenium.By;

public class FlightsPage {

    //*********** By id ***********

    //*********** By class ***********
    public static By yourSelectedFlightTitleClass() {
        return By.className("summary-short-title ng-tns-c114-6");
    }

    //*********** By xpath ***********
    public static String overviewTabXpath() {
        return "//a[contains(text(), ' Overview ')]";
    }

    public static String selectDepartureFlightsDayTabXpath(String day) {
        return "//button/div/div/span[contains(text(), '" + day + "')]";
    }

    public static String flightNumberLabelXpath() {
        return "(//div[contains(text(), 'Flight no.')])[1]";
    }

    public static String flightNumberToExtractLabelXpath() {
        return "(//div[contains(text(), 'Flight no.')])[1]/../div[2]";
    }

    public static String faresTitleXpath() {
        return "//h3[contains(text(), 'Fares')]";
    }

    public static String valuesTitleXpath() {
        return "//h2[contains(text(), ' Value ')]";
    }

    public static String continueWithValueFareButtonXpath() {
        return "//button[contains(text(), ' Continue with Value fare ')]";
    }

    public static String yourSelectedFlightTitleXpath() {
        return "//h3[contains(text(), ' Your selected flight ')]";
    }

    public static String yourSelectedFareTitleXpath() {
        return "//h3[contains(text(), ' Your selected fare ')]";
    }

    public static String loginButtonXpath() {
        return "//button[contains(text(), ' Log in ')]";
    }

    public static String emailTextboxXpath() {
        return "//input[@placeholder='email@email.com']";
    }

    public static String passwordTextboxXpath() {
        return "//input[@placeholder='Password']";
    }

    public static String log_inButtonXpath() {
        return "//button[@type='submit']";
    }

    public static String pleaseEnterNamesAsTheyAppearLabelXpath() {
        return "//p[contains(text(), ' Please enter names as they appear on passport or travel documentation ')]";
    }

    public static String needSpecialAssistanceLabel1Xpath() {
        return "(//span[contains(text(), ' Need special assistance? ')])[1]";
    }

    public static String needSpecialAssistanceLabel2Xpath() {
        return "(//span[contains(text(), ' Need special assistance? ')])[2]";
    }

    public static String needSpecialAssistanceLabel3Xpath() {
        return "(//span[contains(text(), ' Need special assistance? ')])[3]";
    }

    public static String title1DropdownListXpath() {
        return "(//button[@class='dropdown__toggle b2'])[1]";
    }

    public static String title1LabelXpath(String value) {
        return "(//div[contains(text(), '" + value + "')])[1]";
    }

    public static String title2DropdownListXpath() {
        return "(//button[@class='dropdown__toggle b2'])[2]";
    }

    public static String title2LabelXpath(String value) {
        return "(//div[contains(text(), '" + value + "')])[2]";
    }

    public static String title3DropdownListXpath() {
        return "(//button[@class='dropdown__toggle b2'])[3]";
    }

    public static String title3LabelXpath(String value) {
        return "(//div[contains(text(), '" + value + "')])[3]";
    }

    public static String adultPass1FirstnameTextboxXpath() {
        return "//input[@id='form.passengers.ADT-0.name']";
    }

    public static String adultPass2FirstnameTextboxXpath() {
        return "//input[@id='form.passengers.ADT-1.name']";
    }

    public static String childPass1FirstnameTextboxXpath() {
        return "//input[@id='form.passengers.CHD-0.name']";
    }

    public static String adultPass1LastnameTextboxXpath() {
        return "//input[@id='form.passengers.ADT-0.surname']";
    }

    public static String adultPass2LastnameTextboxXpath() {
        return "//input[@id='form.passengers.ADT-1.surname']";
    }

    public static String childPass1LastnameTextboxXpath() {
        return "//input[@id='form.passengers.CHD-0.surname']";
    }

    public static String continueButtonXpath() {
        return "//button[contains(text(), ' Continue ')]";
    }
}
