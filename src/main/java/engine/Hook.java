package engine;

/**
 * @author lionel.mangoua
 * date: 09/09/21
 */

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.PropertyFileReader;
import utils.SeleniumUtility;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class Hook {

    public static PropertyFileReader property = new PropertyFileReader();
    protected static Scenario scenario;
    public static String web_fileName = "web";
    private static boolean root = false;

    //Web
    public static RemoteWebDriver driver;
    public static String BROWSER = "Chrome"; //Firefox, Chrome
    public static WebDriverWait wait = null;
    public static final int WAIT_TIME = 30;
    public static ChromeOptions options;
    public static String chromePath = property.returnPropVal_web(web_fileName, "chromePath");
    public static final Logger LOGGER = getLogger(Hook.class);

    @Before
    public void initialiseDriverAndProperties(Scenario scenario) {

        this.scenario = scenario;

        setupLocalDriver();
    }

    //WEB
    //region <setupLocalDriver>
    public static void setupLocalDriver() {

        log("\nWeb test is Starting ... \n","INFO",  "text");

        if("Firefox".equals(BROWSER)) {
            //setup the firefoxdriver using WebDriverManager dependency
            WebDriverManager.firefoxdriver().setup();

            FirefoxOptions firefoxOptions = new FirefoxOptions();

            driver = new FirefoxDriver(firefoxOptions);
        }
        else if("Chrome".equals(BROWSER)) {
            //setup the chromedriver using WebDriverManager dependency
//            WebDriverManager.chromedriver().setup();

            String chromeAbsolutePath;
            File chromeFile = new File(chromePath);
            System.out.println("Chrome driver directory - " + chromeFile.getAbsolutePath());
            chromeAbsolutePath = chromeFile.getAbsolutePath();

            System.setProperty("webdriver.chrome.driver", chromeAbsolutePath + ""); //setup the chromedriver using chromeDriver in project

            options = new ChromeOptions();

            options.addArguments("--disable-extensions");
            options.addArguments("disable-infobars");
            options.addArguments("test-type");
            options.addArguments("enable-strict-powerful-feature-restrictions");
            options.addArguments("--disable-popup-bloacking");
            options.addArguments("--dns-prefetch-disable"); //TODO new
            options.addArguments("--safebrowsing-disable-auto-update"); //TODO new
            options.addArguments("disable-restore-session-state"); //TODO new
            options.setCapability(ChromeOptions.CAPABILITY, options);
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

            driver = new ChromeDriver(options);
        }

        log("\n'" + BROWSER + "' browser on local machine initiated \n","INFO",  "text");

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, WAIT_TIME);
    }
    //endregion

    //region <logger>
    public static void logger(final String message, final String level, String format) {

        if(format.equalsIgnoreCase(("json"))) {
            String json = (new JSONObject(message)).toString(4); //To convert into pretty Json format
            LOGGER.info("\n" + json); //To print on the console
        }
        else {
            LOGGER.info(message); //To print on the console
        }
    }

    public static void log(final String message, final String level, String format) {

        try {
            logger(message, level, format);
        }
        catch (JSONException err) {
            logger(message, level, "text");
        }
    }

    public static Logger getLogger(Class cls){

        if(root){
            return Logger.getLogger(cls);
        }

        PropertyConfigurator.configure("src/test/resources/log4j.properties"); //path
        root = true;
        return Logger.getLogger(cls);
    }
    //endregion

    @After
    //region <tearDown>
    public void tearDown() {

        try {
            if(driver != null) {
                //To check if the browser is opened
                SeleniumUtility.pause(2000);
                SeleniumUtility.attachScreenShot("Tear down screenshot");
                log("\nTest - WEB is Ending ...\n","INFO",  "text");
                driver.quit();
            }
        }
        catch(Exception ex) {
            log("\nSomething went wrong on test suite tear down!!! ---> " + ex,"INFO",  "text");
        }
    }
    //endregion
}
