package utils;

/**
 * @author lionel.mangoua
 * date: 09/09/21
 */

import engine.Hook;
import junit.framework.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SeleniumUtility extends Hook {

    public static int WaitTimeout = 6;

    //region <attachScreenShot>
    /**
     * To take screenshot
     * and log to Cucumber Report
     */
    public static void attachScreenShot(String description) {

        try {
            final byte[] data = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES); // get screenshot from somewhere
            scenario.attach(data, "image/png", description);
        }
        catch(Exception e) {
            log("Failed to attach screenshot --- " + e.getMessage(), "ERROR",  "text");
        }
    }

    public static void logScreenshot(String description) {

        try {
            String base64Screenshot = "";
            base64Screenshot = "data:image/png;base64," + ((TakesScreenshot) driver).
                    getScreenshotAs(OutputType.BASE64);
            scenario.attach(base64Screenshot, "image/png", description);

            System.out.println("Logged screenshot successfully: " + description);
        }
        catch(Exception e) {
            log("Failed to Log screenshot --- " + e.getMessage(), "ERROR",  "text");
        }
    }
    //endregion

    //region <clearTextByXpath>
    public static void clearTextByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.clear();

            System.out.println("Cleared text on element : " + elementXpath);
            Assert.assertTrue("Cleared text on element : " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to clear text on element --- " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to clear text on element --- " + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <clearTextAndEnterValueByXpath>
    public static void clearTextAndEnterValueByXpath(String elementXpath, String textToEnter, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
            elementToTypeIn.click();
            elementToTypeIn.sendKeys(Keys.chord(Keys.CONTROL, "a")); //Ctrl+a
            elementToTypeIn.sendKeys(Keys.DELETE); //Delete
            Actions typeText = new Actions(driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            elementToTypeIn = driver.findElement(By.xpath(elementXpath));

            elementToTypeIn.click();

            String retrievedText = elementToTypeIn.getAttribute("value");

            if (retrievedText != null) {

                if (retrievedText.equals(textToEnter)) {
                    System.out.println("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
                else {
                    System.out.println("Text entered does not match text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
            }
            else {
                System.out.println("Null value Found");
                Assert.fail("\n[ERROR] Null value Found ---  ");
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clickElementBy>
    public static void clickElementBy(By element, String errorMessage) {

        try {
            WebElement elementToClick = driver.findElement(element);
            elementToClick.click();

            System.out.println("Clicked element : " + element);
            Assert.assertTrue("Clicked element : " + element, true);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to click on element  ---  " + element); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to click on element  ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clickElementByXpath>
    public static void clickElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            System.out.println("Clicked element by Xpath : " + elementXpath);
            Assert.assertTrue("Clicked element by Xpath : " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextByXpath>
    public static void enterTextByXpath(String elementXpath, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                System.out.println("There is No text to enter");
                Assert.assertTrue("There is No text to enter", true);
            }
            else if(textToEnter.equalsIgnoreCase("Clear")) {
                waitForElementByXpath(elementXpath, errorMessage);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.clear();
                Assert.assertTrue("Cleared text field", true);
            }
            else {
                waitForElementByXpath(elementXpath, errorMessage);
                Actions action = new Actions(driver);
                WebElement elementToTypeIn = driver.findElement(By.xpath(elementXpath));
                elementToTypeIn.click();
                action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).perform();
                elementToTypeIn.sendKeys(textToEnter);

                System.out.println("Entered text : \"" + textToEnter + "\" to : " + elementXpath);
                Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + elementXpath, true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath);
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <extractTextByXpath>
    public static String extractTextByXpath(String elementXpath, String errorMessage) {

        String retrievedText = "";
        try {
            WebElement elementToRead = driver.findElement(By.xpath(elementXpath));
            retrievedText = elementToRead.getText();

            log("Extracted text: '" + retrievedText + "' retrieved from element --- " + elementXpath, "ERROR",  "text");
            Assert.assertTrue("Extracted text: " + retrievedText + " retrieved from element --- " + elementXpath, true);

            return retrievedText;
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to retrieve text from element --- " + elementXpath);
            Assert.fail("\n[ERROR] Failed to retrieve text from element --- " + elementXpath + " - " + e.getMessage());

            return retrievedText;
        }
    }
    //endregion

    //region <getAttributeBy>
    public static void getAttributeBy(By element, String attributeType, String errorMessage) {

        try {
            WebElement elementAttributeToGet = driver.findElement(element);
            System.out.println("" + attributeType + " of the element is: ---> " + elementAttributeToGet.getAttribute(attributeType));
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to get attribute of element  ---  " + element); //take screenshot when action fails
            Assert.fail("\n[ERROR] Failed to get attribute of element  ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <pause>
    public static void pause(int millisecondsWait) {

        try {
            Thread.sleep(millisecondsWait);
            log("Paused for " + millisecondsWait + " milliseconds successfully", "ERROR",  "text");
        }
        catch (Exception e) {
            log("Failed to pause", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to pause --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElementByXpath>
    public static void scrollToElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebElement element = driver.findElement(By.xpath(elementXpath));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
            Thread.sleep(500);

            log("Scrolled To Element - " + elementXpath, "ERROR",  "text");
            Assert.assertTrue("Scrolled To Element - " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to scroll To Element - " + elementXpath);
            Assert.fail("\n[ERROR] Failed to scroll To Element - " + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <selectElementFromDropDownListByXpath>
    /*Dynamic method to select value from dropdown list*/
    public static void selectElementFromDropDownListByXpath(String dropdownListXpath, String textToSelect, String errorMessage) {

        try {
            waitForElementByXpath(dropdownListXpath, errorMessage);

            Select dropDownList = new Select(driver.findElement(By.xpath(dropdownListXpath)));
            WebElement formXpath = driver.findElement(By.linkText(textToSelect));
            formXpath.click();
            dropDownList.selectByVisibleText(textToSelect);

            log("Selected Text : " + textToSelect + " from : " + dropdownListXpath, "INFO",  "text");
            Assert.assertTrue("Selected Text : " + textToSelect + " from : " + dropdownListXpath, true);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to select text : " + textToSelect + " from dropdown list by Xpath : " + dropdownListXpath);
            Assert.fail("\n[ERROR] Failed to select text : " + textToSelect + " from dropdown list by Xpath : " + dropdownListXpath + " ---  " + e.getMessage());
        }
    }

    /**
     * This method is use to select value from Dropdown list without first
     * clicking it
     */
    public static void selectFromDropDownListWithoutFirstClickingByXpath(String dropdownListXpath, String valueToSelect, String errorMessage) {

        try {
            waitForElementByXpath(dropdownListXpath, errorMessage);
            Select dropDownList = new Select(driver.findElement(By.xpath(dropdownListXpath)));
            dropDownList.selectByVisibleText(valueToSelect);

            log("Selected Text : " + valueToSelect + " from: " + dropdownListXpath, "INFO",  "text");
            Assert.assertTrue("Selected Text: " + valueToSelect + " from: " + dropdownListXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath : " + dropdownListXpath);
            Assert.fail("\n[ERROR] Failed to select text: " + valueToSelect + " from dropdown list by Xpath  ---  " + e.getMessage());
        }
    }
    //endregion
    
    /*
    //region <selectDropdownValues>
    public static void selectDropdownValues(WebElement ele, String type, String value) {

        try {
            Select sl = new Select(ele);

            switch (type) {
                case "index": {
                    s1.selectByIndex(Integer.parseInt(value));
                    break;
                }
                case "value": {
                    s1.selectByValue(value);
                    break;
                }
                case "text": {
                    s1.selectByVisibleText(value);
                    break;
                }
                default: 
                    System.out.println("Failed to select the valid data!");
                    break;
            }
        }
        catch (Exception e) {
            System.out.println("Failed to select from dropdown");
            Assert.fail("\n[ERROR] Failed to select from dropdown --- " + e.getMessage());
        }
    }
    //endregion
    */

    //region <sendKeys>
    /**
     * This method is used to Press Keys
     */
    public static void sendKeys(String choice) {

        try {
            Actions action = new Actions(driver);

            switch (choice.toUpperCase()) {
                case "ARROW DOWN": {
                    action.sendKeys(Keys.ARROW_DOWN);
                    action.perform();
                    log("Pressed : " + choice, "INFO",  "text");
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
                case "ENTER": {
                    action.sendKeys(Keys.ENTER);
                    action.perform();
                    log("Pressed : " + choice, "INFO",  "text");
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
                case "ESCAPE":
                case "ESC": {
                    action.sendKeys(Keys.ESCAPE);
                    action.perform();
                    log("Pressed : " + choice, "INFO",  "text");
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
                case "TAB": {
                    action.sendKeys(Keys.TAB);
                    action.perform();
                    log("Pressed : " + choice, "INFO",  "text");
                    Assert.assertTrue("Pressed : " + choice, true);
                    break;
                }
            }
        }
        catch (Exception e) {
            log("Failed to send keypress : \"" + choice + "\"", "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to send keypress : \"" + choice);
            Assert.fail("\n[ERROR] Failed to send keypress : \"" + choice + "\" --- " + e.getMessage());
        }
    }
    //endregion

    //region <switchToFrameById>
    public static void switchToFrameById(String frameId) {

        try {
            driver.switchTo().frame(frameId);
            log("Switched to frame '" + frameId + "'", "INFO",  "text");
            Assert.assertTrue("Switched to frame '" + frameId + "'", true);
        }
        catch (Exception e) {
            log("Failed to switch to frame '" + frameId + "'", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to switch to frame '" + frameId + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <switchToFrameByIndex>
    public static void switchToFrameByIndex(int frameNumber) {

        try {
            driver.switchTo().frame(frameNumber);
            log("Switched to frame '" + frameNumber + "'", "INFO",  "text");
            Assert.assertTrue("Switched to frame '" + frameNumber + "'", true);
        }
        catch (Exception e) {
            log("Failed to switch to frame '" + frameNumber + "'", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to switch to frame '" + frameNumber + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <switchToFrameByWebElement>
    public static void switchToFrameByWebElement(By element) {

        try {
            driver.switchTo().frame((WebElement) element);
            log("Switched to frame '" + element + "'", "INFO",  "text");
            Assert.assertTrue("Switched to frame '" + element + "'", true);
        }
        catch (Exception e) {
            log("Failed to switch to frame '" + element + "'", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to switch to frame '" + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <switchToDefaultContent>
    public static void switchToDefaultContent() {

        try {
            driver.switchTo().defaultContent();
            log("Switched to default content", "INFO",  "text");
            Assert.assertTrue("Switched to default content", true);
        }
        catch (Exception e) {
            log("Failed to switch to default content", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to switch to default content - " + e.getMessage());
        }
    }
    //endregion

    //region <waitForElementBoolean>
    /**
     * Method to wait for an element's value. Mostly used in 'if statement' to check
     * if an element is present, without using Assert.fail()
     * */
    public static boolean waitForElementBoolean(By element, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(element)) != null) {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log(errorMessage, "ERROR",  "text");
        }

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return elementFound;
    }
    //endregion

    //region <waitForElement>
    public static void waitForElement(By element, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while(!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 2);

                    wait.until(ExpectedConditions.presenceOfElementLocated(element));
                    wait.until(ExpectedConditions.elementToBeClickable(element));
                    if(wait.until(ExpectedConditions.visibilityOfElementLocated(element)) != null) {
                        elementFound = true;
                        System.out.println("Found element : " + element);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    log("Did Not Find element: " + element + "' - " + e.getMessage(), "ERROR",  "text");
                }
                waitCount++;
            }
            if(waitCount == WaitTimeout) {
                returnElementFound(elementFound);
                log(errorMessage, "ERROR",  "text");
                attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element --- " + element);
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + element);
            }
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to wait for element --- " + element);
        }

        returnElementFound(elementFound);
    }

    private static boolean returnElementFound(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForElementByXpath>
    public static void waitForElementByXpath(String elementXpath, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null) {
                        elementFound = true;
                        System.out.println("Found element by Xpath : " + elementXpath);
                        Assert.assertTrue("Found element by Xpath : " + elementXpath, true);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    log("Did Not Find element by Xpath: " + elementXpath, "ERROR",  "text");
                }
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1(elementFound);
                log(errorMessage, "ERROR",  "text");
                attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath: '" + elementXpath);
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath: '" + elementXpath + "'");
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath);
            Assert.fail("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath + "' - " + e.getMessage());
        }

        GetElementFound(elementFound);
    }

    private static boolean GetElementFound(boolean elementFound) {
        return elementFound;
    }

    private static boolean GetElementFound1(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForElementToBeClickableByXpath>
    public static void waitForElementToBeClickableByXpath(String elementXpath, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));

            elementFound = true;
            System.out.println("Found element : " + elementXpath);
        }
        catch(Exception e) {
            returnElementFound(elementFound);
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath);
        }

        returnElementFound(true);
    }
    //endregion

    //region <waitForElementToBeLocatedBy>
    public static void waitForElementToBeLocatedBy(By element, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(element));

            elementFound = true;
            System.out.println("Found element : " + element);
        }
        catch(Exception e) {
            returnElementFound(elementFound);
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element --- " + element); //take screenshot when action fails
            Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + element);
        }

        returnElementFound(true);
    }
    //endregion

    //region <waitForElementToBeLocatedByXpath>
    public static void waitForElementToBeLocatedByXpath(String elementXpath, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));

            elementFound = true;
            System.out.println("Found element : " + elementXpath);
        }
        catch(Exception e) {
            returnElementFound(elementFound);
            log(errorMessage, "ERROR",  "text");
            attachScreenShot("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath); //take screenshot when action fails
            Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + elementXpath);
        }

        returnElementFound(true);
    }
    //endregion
}
